## Sprint 1

### Pianificaizione

Durante la pianificazione dello sprint 1 abbiamo deciso che
la cosa migliore da fare sarebbe stata quella di iniziare a
lavorare sulle funzionalità di base, ma comunque di rendere 
la partita di scacchi giocabile. Abbiamo quindi definito
lo schema del database e abbiamo lavorato sul routing e sulle
view di base. Abbiamo scelto framework e librerie adatti per
lo sviluppo del progetto e abbiamo provato a presentare un piccolo prototipo.

### Sprint Goal

L'obiettivo che ci siamo posti per lo sprint 1 è quello di realizzare un prototipo
di app giocabile contro il bot, con un database funzionante e con un'interfaccia
basilare, ma che permetta di potersi autenticare e salvare i risultati di una partita.
È possibile giocare anche senza autenticazione ma i progressi non verranno salvati.

### Definition of Ready

Il product backlog dello sprint 1 presenta task di dimensioni
variabili e alcune di queste sono molto corpose. Le task sono
state pensate in modo da potersi assicurare che tutti avessero sempre
qualcosa da fare e che nessuno rimanesse fermo. Abbiamo cercato di
definire task che fossero chiare e che non lasciassero spazio a dubbi
sulla loro implementazione e che fossero indipendenti tra loro in modo
da poter essere sviluppate in parallelo. Data la poca esperienza di collaborazione
con i membri del gruppo, la stesura delle task non è stata semplice ed è
stata fatta un po' alla cieca non conoscendo la produttività dei singoli.

### Definition of Done (per task)
- Test sulle funzionalità del backend e test sulle funzionalità del frontend superati
- Analisi del codice effettuata
- Refactoring del codice effettuato
- Documentazione del codice se necessaria
- Task approvata dal team di sviluppo


## Retrospective
L'analisi della retrospective è principalmente descritta nel pdf chiamato 
retrospective-star.pdf, presente nella stessa cartella di questo file.

Di seguito ci sono le risposte ad alcune domande che abbiamo trovato utili nell'analisi
della retrospettiva.


### Cosa proviamo riguardo al prossimo sprint ora che abbiamo identificato questi problemi?

Dopo il primo sprint ci siamo conosciuti meglio e abbiamo scoperto le potenzialità e i
limiti del team. Guardando al futuro abbiamo deciso di prendere delle decisioni che possono
migliorare l'ambiente di sviluppo, per esempio: 
- Utilizzare delle convenzioni per la stesura del codice, abbiamo scelto di utilizzare react-bootstrap come libreria per lo 
sviluppo frontend;
- Fare più commit prima della chiusura di un issue in modo da poter avere un maggior controllo sul codice che viene scritto;
- Non ridursi all'ultimo per concludere la task.
- Ricordarsi di chiudere le issue ed eliminare i branch per le task che sono state concluse.
- Utilizzare sonarqube per la verifica del codice.


### Cosa abbiamo imparato?

Abbiamo imparato che bisogna scrivere sempre la documentazione e i test prima di
concludere una task, abbiamo imparato ad utilizzare strumenti come taiga e mattermost
che ci hanno reso l'esperienza lavorativa più semplice e organizzata. Abbiamo iniziato
a prendere confidenza con il metodo scrum.


### Siamo d'accordo su quali azioni intraprenderemo da questa retrospettiva dello sprint alla prossima pianificazione dello sprint?
Intraprenderemo sicuramente un'analisi più approfondita delle task da svolgere e la loro divisione ora che conosciamo meglio
le potenzialità dei singoli membri, inoltre ci assicureremo di far si che tutti siano presenti alle riunioni dello sprint planning
in modo che tutti capiscano bene quali sono i loro compiti e in modo da chiarire tutti i dubbi che sorgono durante la pianificazione.