<?php


/************ Routes From Main **************/

test("can navigate from main to register", function () {
    $response = $this->get('/');
    $response->assertStatus(200);
    $response->assertSee('Register');
    $response = $this->get('/register');
    $response->assertStatus(200);
})->only();

test("can navigate from main to login", function () {
    $response = $this->get('/');
    $response->assertStatus(200);
    $response->assertSee('Login');
    $response = $this->get('/login');
    $response->assertStatus(200);
});

test("can navigate from main to dashboard without auth", function () {
    $response = $this->get('/');
    $response->assertStatus(200);
    $response->assertSee('Continue without registration');
    $response = $this->get('/dashboard');
    $response->assertStatus(200);
});

/************ Routes From Register **************/

test("can navigate from register to login", function () {
    $response = $this->get('/register');
    $response->assertStatus(200);
    $response->assertSee('Already registered?');
    $response = $this->get('/login');
    $response->assertStatus(200);
});

/************ Routes From Login **************/

test("can navigate from login to register", function () {
    $response = $this->get('/login');
    $response->assertStatus(200);
    $response->assertSee('Not yet registered?');
    $response = $this->get('/register');
    $response->assertStatus(200);
});


/************ Routes From Dashboard **************/

test("can navigate from dashboard to game", function () {
    $response = $this->get('/dashboard');
    $response->assertStatus(200);
    $response->assertSee("Gioca");
    $response = $this->get('/game');
    $response->assertStatus(200);
});



