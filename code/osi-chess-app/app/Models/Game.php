<?php

namespace App\Models;

use App\Enums\GameStatusEnum;
use Chess\FenToBoard;
use Chess\Play\SanPlay;
use Chess\UciEngine\Stockfish;
use Chess\Variant\Classical\Board;
use Chess\Variant\Classical\PGN\AN\Color;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function Amp\Promise\first;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'status',
        'room_code',
        'pgn',
        'last_fen',
        'type',
        'move_text',
        'initial_fen',
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function gameInfos(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(GameInfo::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Helper functions
    |--------------------------------------------------------------------------
    */
    public function generateInitialPgn(): void
    {
        $this->pgn = "[Site \"Osi-Chess\"]\n
        [Date '" . date('Y.m.d') . "']\n
        [FEN \"" . self::generateInitialFen() . "\"]\n";
        $this->save();
    }




    public function generateInitialFen(): void
    {
        function generateRandomFENString() {
            // Initialize pieces without kings
            $whitePieces = ["P", "N", "B", "R", "Q"];
            $blackPieces = ["p", "n", "b", "r", "q"];

            // Initialize FEN arrays
            $whiteFen = [];
            $blackFen = [];

            // Generate a number within the range 0-15 to place kings
            $whiteKingPos = 8 + rand(0, 7);
            $blackKingPos = rand(0, 7);

            // Iterate over 16 squares
            for ($i = 0; $i < 16; $i++) {
                // Generate a random piece for both white and black
                $whitePiece = $whitePieces[array_rand($whitePieces)];
                $blackPiece = $blackPieces[array_rand($blackPieces)];

                // If the random number generated is the same as the king position, place the king
                if ($i === $whiteKingPos) {
                    $whitePiece = "K";
                }
                if ($i === $blackKingPos) {
                    $blackPiece = "k";
                }

                // Push the random piece to the FEN array if not king
                $whiteFen[] = $whitePiece;
                $blackFen[] = $blackPiece;
            }

            // Join the FEN arrays and add the slashes
            $whiteFen = implode("", $whiteFen);
            $whiteFen = substr($whiteFen, 0, 8) . "/" . substr($whiteFen, 8, 8);
            $blackFen = implode("", $blackFen);
            $blackFen = substr($blackFen, 0, 8) . "/" . substr($blackFen, 8, 8);

            // Return the FEN string
            return $blackFen . "/8/8/8/8/" . $whiteFen . " w - - 0 1";
        }

        // TODO: actually generate initial FEN
        $board = FenToBoard::create(generateRandomFENString());
        $this->initial_fen = $board->toFen();
        $this->last_fen = $this->initial_fen;
        $this->save();
    }

    public function playAiMove(): void
    {
        // Load status from pgn
        $board = $this->getBoard();

        // Initialize stockfish
        $stockfish = (new Stockfish($board))
            ->setOptions([
                'Skill Level' => 9
            ])
            ->setParams([
                'depth' => 3
            ]);

        // Play move
        $lan = $stockfish->play($board->toFen());
        $board->playLan('b', $lan);

        //check if game is over
        if(($board->isMate() || $board->isStalemate()) && $this->status != GameStatusEnum::BLACK_WIN && $this->status != GameStatusEnum::WHITE_WIN) {
            $this->status = GameStatusEnum::BLACK_WIN;

            //check if user is logged
            if(User::isLoggedIn()) {
                $game_info = $this->gameInfos()->where('user_id', User::getLoggedUser()->id)->first();
                $game_info->winner = 0;
                $game_info->save();
            }
        }

        // Save to database
        $this->saveBoardOnDb($board);
    }

    public function getBoard()
    {
        // Get game from database
        $game = Game::find($this->id);

        // Check if move_text is null
        if($game->move_text == null) {

            // If move_text is null, this is the first move, just create board from initial FEN
            return FenToBoard::create($game->initial_fen);
        }
        else
        {
            // Create board from initial FEN and play move_text
            $board = FenToBoard::create($game->initial_fen);
            return (new SanPlay($game->move_text,$board))
                ->validate()
                ->getBoard();
        }
    }

    /**
     * Plays move as logged user on current game
     * @param $move
     * @return void
     */
    public function playMove($move): void
    {
        // Check if user is logged
        $userIsLogged = User::isLoggedIn();

        // Get player color
        $playerColor = !$userIsLogged ? 'w' : // If user is not logged, player color is white

            // If user is logged get player color from database
            $this->gameInfos
                ->where('user_id', User::getLoggedUser()->id)
                ->first()
                ->color;

        // Get board
        $board = $this->getBoard();

        // Play move
        $board->playLan($playerColor, $move);

        // Get game info
        $game_info = null;
        if ($userIsLogged) {
            $game_info = $this->gameInfos()->where('user_id', User::getLoggedUser()->id)->first();
        }

        // Check if game is over
        if($board->isMate() || $board->isStalemate() && $this->status != GameStatusEnum::BLACK_WIN && $this->status != GameStatusEnum::WHITE_WIN) {
            $this->status = GameStatusEnum::WHITE_WIN;
            if ($userIsLogged) {
                $game_info->winner = 1;
            }
        }

        if ($userIsLogged) {
            $game_info->moves_count++;
            $game_info->save();
        }


        // Save to database
        $this->saveBoardOnDb($board);
    }

    public function saveBoardOnDb($board): void
    {
        // Update game last FEN
        $this->last_fen = $board->toFen();

        // Update move text
        $this->move_text = $board->getMovetext();

        // Build pgn with FEN and move text
        $this->pgn = "[Site \"Osi-Chess\"]\n[FEN \"" . $this->initial_fen . "\"]\n" . $this->move_text;

        // Save game
        $this->save();
    }
}
