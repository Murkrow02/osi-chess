<?php

namespace App\Http\Controllers;

use App\Enums\GameTypesEnum;
use App\Http\Requests\NewMoveRequest;
use App\Models\Game;
use Chess\FenToBoard;
use Chess\Variant\Classical\PGN\AN\Color;
use App\Enums\GameStatusEnum;
use App\Http\Requests\CreateGameRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Inertia\Inertia;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Create new game
     * @param CreateGameRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CreateGameRequest $request)
    {
        // Create new game
        $game = Game::create([
            'status' => GameStatusEnum::NO_STATUS,
            'room_code' => 'TODO:' . rand(1000, 9999),
            'type' => $request->type,
        ]);

        // If guest, do not create game info object (used only for stats)
        if (User::isLoggedIn()) {

            // Create game info for white player
            $game->gameInfos()->create([
                'user_id' => User::getLoggedUser()->id,
                'color' => Color::W,
            ]);
        }

        // Generate initial FEN
        $game->generateInitialFen();

        // Black player infos are created when black player joins the game

        // Now redirect to game page
        return Inertia::location('/games/' . $game->id);
    }

    /**
     * Load the game page when game is already created
     * @param $gameId
     * @return \Inertia\Response
     */
    public function show($gameId)
    {
        // Get game object
        $game = Game::find($gameId);
        $game->load('gameInfos');

        // Create black player only if
        if ($game->type == GameTypesEnum::PVP                                   // game is PVP
            && $game->gameInfos->count() == 1                                   // black player is not already present
            && $game->gameInfos->first()->user_id != User::getLoggedUser()->id) // requesting user is not already a player
        {
            // Create game info for black player
            $game->gameInfos()->create([
                'user_id' => User::getLoggedUser()->id,
                'color' => 'black',
            ]);
        }

        // Return game page
        return Inertia::render('Game', [
            'game' => $game
        ]);
    }

    /**
     * Post new move and return updated game state (if versus AI then AI will make a move)
     * @return JsonResponse
     */
    public function move(NewMoveRequest $request, $gameId)
    {

        // Get game
        $game = Game::find($gameId);

        // Play requested move
        $game->playMove($request->move);


        // TODO: validate move

        if ($game->status == GameStatusEnum::WHITE_WIN || $game->status == GameStatusEnum::BLACK_WIN) {
            //TODO verifica se dal client si triggera la fine anche senza fare niente qui

            // Return updated game state
            return response()->json($game);
        }

        // Play AI move if game is versus AI
        if ($game->type == GameTypesEnum::PVB->value) {
            $game->playAiMove();
        }

        // Return updated game state
        return response()->json($game);
    }
}
