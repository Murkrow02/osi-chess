<?php

namespace App\Http\Requests;

use App\Enums\GameTypesEnum;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string type
 */
class CreateGameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'type' => 'required|in:' . implode(',', GameTypesEnum::values()),
        ];
    }
}
