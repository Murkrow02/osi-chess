<?php

use App\Http\Controllers\GameController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Dashboard');
    //return Inertia::render('Welcome');
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');


Route::get('game', function () {
    return Inertia::render('Game');
})->name('game');

Route::get('test', function(){
    return Inertia::render('TestPage');
});

/*
|--------------------------------------------------------------------------
| Profile
|--------------------------------------------------------------------------
*/
Route::group([
    'middleware' => 'auth',
    'prefix' => 'profile'
], function () {
    Route::get('/', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::get('/stats', [ProfileController::class, 'stats'])->name('profile.stats');
    Route::patch('/', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

/*
|--------------------------------------------------------------------------
| Game
|--------------------------------------------------------------------------
*/
Route::group([
    /*'middleware' => 'auth',*/
    'prefix' => 'games'
], function () {

    // Create new game
    Route::post('/', [GameController::class, 'create']);

    // Load game page
    Route::get('/{gameId}', [GameController::class, 'show']);

    // Post new move and return updated game state (if versus AI then AI will make a move)
    Route::post('/{gameId}/move', [GameController::class, 'move']);

    Route::get('/test', [GameController::class, 'test']);
});


require __DIR__.'/auth.php';
