<?php

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\ServiceProvider;

return [
    'game_count' => 10,
    'user_count' => 10,
];
