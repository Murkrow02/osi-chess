export default function (props)
{
    return (
        <h2 className='label label-big'>{props.children}</h2>
    );
}
