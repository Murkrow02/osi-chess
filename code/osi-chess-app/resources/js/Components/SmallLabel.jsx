export default function (props)
{
    return (
        <p className='label label-small'>{props.children}</p>
    );
}
