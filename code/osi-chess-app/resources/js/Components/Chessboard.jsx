import React, { Component } from "react";
import PropTypes from "prop-types";
import { Chess } from "chess.js"; // import Chess from  "chess.js"(default) if recieving an error about new Chess() not being a constructor

import Chessboard from "chessboardjsx";
import {parse} from "@mliebelt/pgn-parser";

const HIGHLIGHT_COLOR = "#ffb3b3aa";

class HumanVsBot extends Component {
    static propTypes = { children: PropTypes.func };
    initialFen = "";
    oldFen = "";
    newFen = "";
    gameModel = this.props.game;

    state = {
        fen: this.props.position,
        // square styles for active drop square
        dropSquareStyle: {},
        // custom square styles
        squareStyles: {},
        // square with the currently clicked piece
        pieceSquare: "",
        // currently clicked square
        square: "",
        // array of past game moves
        history: []
    };

    componentDidMount() {
        this.game = new Chess(this.props.position);
        this.oldFen = this.props.position;
        this.initialFen = this.props.position;

        //check if the pgn is created correctly
        if (this.props.game.pgn != null) {
            //get pgn from model
            let pgn = this.props.game.pgn;
            //parse pgn to get the moves
            let parsedPGN = parse(pgn, {startRule: "game"}).moves;
            //check that moves are made
            if (parsedPGN.length > 0) {

                for (let i = 0; i < parsedPGN.length; i++) {
                    //apply moves to the board
                    let move_obj = this.game.move(parsedPGN[i].notation.notation);
                    console.log(move_obj.from + " " + move_obj.to);
                }

                //update the board and history
                this.setState(({ history, pieceSquare }) => ({
                    fen: this.game.fen(),
                    history: this.game.history({ verbose: true }),
                    squareStyles: squareStyling({ pieceSquare, history })
                }));

                this.onMove();
            }
        }
    }

    // keep clicked square style and remove hint squares
    removeHighlightSquare = () => {
        this.setState(({ pieceSquare, history }) => ({
            squareStyles: squareStyling({ pieceSquare, history })
        }));
    };
    // show possible moves
    highlightSquare = (sourceSquare, squaresToHighlight) => {
        const highlightStyles = [sourceSquare, ...squaresToHighlight].reduce(
            (a, c) => {
                return {
                    ...a,
                    ...{
                        [c]: {
                            backgroundColor: HIGHLIGHT_COLOR,
                        }
                    },
                    ...squareStyling({
                        history: this.state.history,
                        pieceSquare: this.state.pieceSquare
                    })
                };
            },
            {}
        );

        this.setState(({ squareStyles }) => ({
            squareStyles: { ...squareStyles, ...highlightStyles }
        }));
    };
    handleStepBackButtonStatus = () => {

        let undoButton = document.querySelector(".undo-button");
        //check if the fen is the initial one, or it's the black turn
        if (undoButton == null) {
            return;
        }

        if (this.initialFen === this.game.fen() || this.game.turn() === "b") {
            if (!undoButton.classList.contains("disabled")) {
                undoButton.classList.add("disabled");
            }
        } else {
            if (undoButton.classList.contains("disabled")) {
                undoButton.classList.remove("disabled");
            }
        }
    }
    getPiecePositions = (piece) => {
        return [].concat(...this.game.board()).map((p, index) => {
            if (p !== null && p.type === piece.type && p.color === piece.color) {
                return index
            }
        }).filter(Number.isInteger).map((piece_index) => {
            const row = 'abcdefgh'[piece_index % 8]
            const column = Math.ceil((64 - piece_index) / 8)
            return row + column
        })
    }
    handleCheck = () => {
        function removeCheckClass() {
            if (document.querySelector(".check") != null) {
                document.querySelector(".check").classList.remove("check");
            }
        }

        if (this.game.isCheck()){
            //start removing the class
            removeCheckClass();

            //check which color is in check
            let kingPos = "";
            if (this.game.turn() === "w"){
                console.log("White is in check");
                kingPos = this.getPiecePositions({type: "k", color: "w"});
            } else {
                console.log("Black is in check");
                kingPos = this.getPiecePositions({type: "k", color: "b"});
            }

            let kingEl = document.querySelector("div[data-squareid='" + kingPos + "']");
            if (kingEl != null) {
                kingEl.classList.add("check");
            }

        } else {
            removeCheckClass();
        }
    }

    requestMove = (moveLan) => {

        let page = this;

        axios.post('/games/' + this.gameModel.id + '/move', {
            move: moveLan
        }).then(function (response) {
            console.log(response);
           let content = response.data;
           let updatedPgn = content.pgn;
           let lastMove = parse(updatedPgn, {startRule: "game"}).moves[parse(updatedPgn, {startRule: "game"}).moves.length - 1].notation.notation;
           page.game.move(lastMove);

           page.setState(({ history, pieceSquare }) => ({
               fen: page.game.fen(),
               history: page.game.history({ verbose: true }),
               squareStyles: squareStyling({ pieceSquare, history })
           }));

            page.onMove();
           console.log(lastMove);
        }).catch(function (error) {
            console.log(error);
        });
    }
    onMove = () => {
        this.handleStepBackButtonStatus();

        this.oldFen = this.state.fen;
        this.newFen = this.game.fen();

        //check if it's checkmate
        if (this.game.isCheckmate()) {
            //check if white is in checkmate
            if (this.game.turn() === "w") {
                alert("Checkmate! You lost!");
            } else {
                alert("Checkmate! You won!");
            }
        }

        console.log(this.game.history())

        this.handleCheck();
    }

    stepBack = () => {
        //check if the fen is the initial one
        if (this.oldFen === this.state.fen) {
            console.log("initial fen")
            return;
        }
        //check if it is white's turn
        if (this.game.turn() === "b") {
            console.log("black's turn")
            return;
        }

        //undo the move
        this.game.undo();
        this.game.undo();
        this.setState(({ history }) => ({
            fen: this.game.fen(),
            history: this.game.history({ verbose: true }),
        }));

        //signal that the move was made
        this.onMove();
    }
    onDrop = ({ sourceSquare, targetSquare }) => {



        //Handle move validation => it crashes the app without it
        try {
            // see if the move is legal
            let move = this.game.move({
                from: sourceSquare,
                to: targetSquare,
                promotion: "q" // always promote to a queen for example simplicity
            });
        }
        catch (err) {
            return;
        }


        //update the board and history
        this.setState(({ history, pieceSquare }) => ({
            fen: this.game.fen(),
            history: this.game.history({ verbose: true }),
            squareStyles: squareStyling({ pieceSquare, history })
        }));
        this.onMove();

        this.requestMove(sourceSquare + targetSquare);
    };
    onMouseOverSquare = square => {

        // get list of possible moves for this square
        let moves = this.game.moves({
            square: square,
            verbose: true
        });

        // exit if there are no moves available for this square
        if (moves.length === 0) {
            //TODO highlight only the square
            return;
        }

        let squaresToHighlight = [];
        for (var i = 0; i < moves.length; i++) {
            squaresToHighlight.push(moves[i].to);
        }
        this.highlightSquare(square, squaresToHighlight);
    };
    onMouseOutSquare = square => this.removeHighlightSquare(square);
    // central squares get diff dropSquareStyles
    onDragOverSquare = square => {
        this.setState({
            dropSquareStyle: {boxShadow: "inset 0 0 1px 4px rgb(255, 255, 0)"}
        });
    };
    onSquareClick = square => {

        this.setState(({ history }) => ({
            squareStyles: squareStyling({ pieceSquare: square, history }),
            pieceSquare: square
        }));

        //check if the source square is empty
        if (this.state.pieceSquare.toString() === "") {
            console.log("empty square clicked: " + square);
            //timer of 100ms
            setTimeout(() => {
                if (this.state.pieceSquare.toString() === "") {
                    return;
                }
            }, 100);
        }

        //non-empty square clicked
        console.log("square clicked: " + square);
        this.onMouseOverSquare(square);

        //check if the square is the same as the last one
        if (this.state.pieceSquare === square) {
            return;
        }

        try {
            let move = this.game.move({
                from: this.state.pieceSquare,
                to: square,
                promotion: "q" // always promote to a queen for example simplicity
            });
        }
        catch (err) {
            return;
        }

        this.setState({
            fen: this.game.fen(),
            history: this.game.history({ verbose: true }),
            pieceSquare: ""
        });

        this.onMove();

        this.requestMove(this.state.pieceSquare + square)
    };
    render() {
        const { fen, dropSquareStyle, squareStyles } = this.state;

        return <div className={"human-vs-human-container"}>
            {[this.props.children[0]({
            squareStyles,
            position: fen,
            onMouseOverSquare: this.onMouseOverSquare,
            onMouseOutSquare: this.onMouseOutSquare,
            onDrop: this.onDrop,
            dropSquareStyle,
            onDragOverSquare: this.onDragOverSquare,
            onSquareClick: this.onSquareClick,
        }), this.props.children[1]({
            stepBack: this.stepBack
        })]}</div>;
    }
}

export default function GameWrapper(props) {

    return (
        <div>
            <HumanVsBot style={{color:"red"}} position={props.position} game={props.game} >
                {({
                      position,
                      onDrop,
                      onMouseOverSquare,
                      onMouseOutSquare,
                      squareStyles,
                      dropSquareStyle,
                      onDragOverSquare,
                      onSquareClick,
                      onSquareRightClick,
                      allowDrag
                  }) => (
                    <Chessboard
                        id="humanVsHuman"
                        width={320}
                        position={position}
                        onDrop={onDrop}
                        onMouseOverSquare={onMouseOverSquare}
                        onMouseOutSquare={onMouseOutSquare}
                        boardStyle={{
                            borderRadius: "5px",
                            boxShadow: `0 5px 15px rgba(0, 0, 0, 0.5)`
                        }}
                        squareStyles={squareStyles}
                        dropSquareStyle={dropSquareStyle}
                        onDragOverSquare={onDragOverSquare}
                        onSquareClick={onSquareClick}
                        onSquareRightClick={onSquareRightClick}
                    />
                )}
                {({ stepBack }) => (
                    <button className={`undo-button disabled`} onClick={stepBack}>Undo</button>
                )}
            </HumanVsBot>
        </div>
    );
}

const squareStyling = ({ pieceSquare, history }) => {
    const sourceSquare = history.length && history[history.length - 1].from;
    const targetSquare = history.length && history[history.length - 1].to;

    return {
        [pieceSquare]: { backgroundColor: "rgba(255, 255, 0, 0.4)" },
        ...(history.length && {
            [sourceSquare]: {
                backgroundColor: "rgba(255, 255, 0, 0.4)"
            }
        }),
        ...(history.length && {
            [targetSquare]: {
                backgroundColor: "rgba(255, 255, 0, 0.4)"
            }
        })
    };
};
