import {useEffect} from 'react';
import Checkbox from '@/Components/Checkbox';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import {Head, Link, useForm} from '@inertiajs/react';
import '../../../css/LoginFormStyles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import SmallLabel from "@/Components/SmallLabel.jsx";
import BigLabel from "@/Components/BigLabel.jsx";

export default function Login({status, canResetPassword}) {

    const {data, setData, post, processing, errors, reset} = useForm({
        username: '',
        password: '',
        remember: false,
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('login'));
    };

    return (
        <div className='login-wrapper'>
            <Head title="Log in"/>

            <div className={'login-form'}>
                <img src='/images/loghetto.png' alt="error" className="login-logo"></img>
                <BigLabel>Osichess</BigLabel>

                <Link href={route('dashboard')} className='login-link'>
                    <PrimaryButton disabled={processing} style={{width: '100%'}}>
                        Continue without login
                    </PrimaryButton>
                </Link>

                <div className="login-bho">
                    <div className="login-line"></div>
                    <span className="login-or">Or</span>
                    <div className="login-line"></div>
                </div>

                <form onSubmit={submit}>

                    <Container>

                        <Row>

                            <TextInput
                                id="username"
                                type="text"
                                name="username"
                                placeholder="Username"
                                value={data.username}
                                autoComplete="username"
                                isFocused={true}
                                onChange={(e) => setData('username', e.target.value)}

                            />

                            <InputError message={errors.username}/>
                        </Row>

                        <Row>

                            <TextInput
                                id="password"
                                type="password"
                                name="password"
                                placeholder="Password"
                                value={data.password}
                                autoComplete="current-password"
                                onChange={(e) => setData('password', e.target.value)}
                            />

                            <InputError message={errors.password}/>
                        </Row>
                        <Row>
                            <PrimaryButton disabled={processing} type={'submit'}>
                                Log in
                            </PrimaryButton>
                        </Row>

                        <Row>
                            <SmallLabel>Don't have an account yet?<Link
                                href={route('register')}> Register</Link>
                            </SmallLabel>
                        </Row>
                    </Container>
                </form>
            </div>
        </div>
    );
}
