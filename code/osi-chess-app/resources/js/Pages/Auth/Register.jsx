import {useEffect} from 'react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import {Head, Link, useForm} from '@inertiajs/react';
import '../../../css/RegisterStyle.css';
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import BigLabel from "@/Components/BigLabel.jsx";
import SmallLabel from "@/Components/SmallLabel.jsx";


export default function Register() {
    const {data, setData, post, processing, errors, reset} = useForm({
        name: '',
        username: '',
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const submit = (e) => {
        e.preventDefault();

        post(route('register'));
    };

    return (
        <div className='register-wrapper'>
            <Head title="Register"/>

            <form onSubmit={submit}>
                <Container>

                    <Row>
                        <img src="/images/loghetto.png" alt="error" className="register-logo"></img>
                    </Row>

                    <Row>
                        <BigLabel>Create an account</BigLabel>
                    </Row>

                    <Row>
                        <TextInput
                            id="username"
                            type="text"
                            name="username"
                            placeholder='username'
                            value={data.username}
                            autoComplete="username"
                            onChange={(e) => setData('username', e.target.value)}
                            required
                        />

                        <InputError message={errors.username} className="mt-2"/>
                    </Row>

                    <Row>

                        <TextInput
                            id="password"
                            type="password"
                            name="password"
                            placeholder='password'
                            value={data.password}
                            autoComplete="new-password"
                            onChange={(e) => setData('password', e.target.value)}
                            required
                        />

                        <InputError message={errors.password} className="mt-2"/>
                    </Row>

                    <Row>

                        <TextInput
                            id="password_confirmation"
                            type="password"
                            name="password_confirmation"
                            placeholder='confirm password'
                            value={data.password_confirmation}
                            autoComplete="new-password"
                            onChange={(e) => setData('password_confirmation', e.target.value)}
                            required
                        />

                        <InputError message={errors.password_confirmation} className="mt-2"/>
                    </Row>

                    <Row>
                        <PrimaryButton disabled={processing}>
                            Register
                        </PrimaryButton>
                    </Row>

                    <Row>
                        <SmallLabel> Already registered?
                            <Link
                                href={route('login')}
                                className="register-link">
                                Log in
                            </Link></SmallLabel>
                    </Row>
                </Container>
            </form>
        </div>
    );
}
