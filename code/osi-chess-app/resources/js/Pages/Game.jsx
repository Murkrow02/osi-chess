import React, {useState, useEffect} from 'react';
import Chessboard from '@/Components/Chessboard';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import '../../css/game.css';
import {func} from "prop-types";
import {parse} from '@mliebelt/pgn-parser'

export default function Game({auth, game}) {

    // VALE VEDI DOVE DEVE ANDARE QUESTO IO NON LO SO DEVE FARLO UNA SOLA VOLTA A COMPONENTE MONTATO
    // FRA LO TANTO ME LO PASSI NEL MODELLO GAME QUINDI LO TENGO SEMPRE DISPONIBILE DENTRO LA SCACCHIERA
    // MA IO DICEVO IL FATTO CHE ASCOLTA I WEBSOCKETS
    let gameId = 1; //TODO: get gameId from API
    /*Echo.private(`game.${gameId}`)
        .listen('NewMoveEvent', (e) => {
            console.log(e);
        });*/


    // Get the initial FEN string from the game object, if this is the first move a PGN string is not present so we get the last FEN string
    let initialFen = game.pgn ?
        parse(game.pgn, {startRule: "game"}).tags.FEN :
        game.initial_fen;

    console.log(game);


    /**PRATICAMENTE COME FUNZIONA**/
    // Questa pagina può essere invocata in due modi diversi:
    // Se viene effettuata la /api/games viene creata una nuova partita e viene ritornato modello della nuova partita contenente la pgn vuota
    // Se viene effettuata la /api/games/{id} viene ritornato il modello della partita con id {id} con la pgn aggiornata

    // Per mandare una mossa è necessario chiamare la /api/games/{id}/move con il body {move: "e4e6"} dove move è la LAN della mossa da effettuare
    // Il server risponderà con il modello della partita aggiornato e la mossa lan effettuata dall'avversario.


    return (
        <AuthenticatedLayout user={auth.user}>
            {/*
            SUMMARY:
            - The Game component is the main component of the app.
            - It contains the board and the pieces.
            - It also contains the logic of the game.

            \param position: the START position of the pieces on the board.
                It could be a FEN string or a piece array object.
            */}
            <div className={'game-wrapper'}>
                <Chessboard position={initialFen} game={game}/>
            </div>
        </AuthenticatedLayout>
    );


    // Generate a random FEN string, ONLY 1 KING ALLOWED PER SIDE
    function generateRandomFENString() {
        //Init pieces without kings
        const whitePieces = ["P", "N", "B", "R", "Q"];
        const blackPieces = ["p", "n", "b", "r", "q"];
        //Init FEN arrays
        let whiteFen = [];
        let blackFen = [];
        //generate number within range 0-15 to place kings
        let whiteKingPos = 8 + Math.floor(Math.random() * 8);
        let blackKingPos = Math.floor(Math.random() * 8);

        //Iterate over 16 squares
        for (let i = 0; i < 16; i++) {
            //Generate random piece for both white and black
            let whitePiece = whitePieces[Math.floor(Math.random() * 5)];
            let blackPiece = blackPieces[Math.floor(Math.random() * 5)];

            //If the random number generated is the same as the king position, place the king
            if (i === whiteKingPos) {
                whitePiece = "K";
            }
            if (i === blackKingPos) {
                blackPiece = "k";
            }

            //Push the random piece to the FEN array if not king
            whiteFen.push(whitePiece);
            blackFen.push(blackPiece);
        }

        //Join the FEN arrays and add the slashes
        whiteFen = whiteFen.join("")
        whiteFen = whiteFen.slice(0, 8) + "/" + whiteFen.slice(8, 16);
        blackFen = blackFen.join("")
        blackFen = blackFen.slice(0, 8) + "/" + blackFen.slice(8, 16);

        //Return the FEN string
        return blackFen + "/8/8/8/8/" + whiteFen + " w - - 0 1"
    }
}
