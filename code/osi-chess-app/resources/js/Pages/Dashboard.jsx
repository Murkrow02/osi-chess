import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import {Head, Link, router} from '@inertiajs/react';
import TestForm from "@/Components/TestForm.jsx";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import '../../css/NewGame.css';
import Image from 'react-bootstrap/Image';
import Form from 'react-bootstrap/Form';

export default function Dashboard({ auth }) {

    function newGame() {

        router.post('/games', {
                time: 10,
                type: 'pvb',
            },
            {
                onError: () => {
                    alert('error');
                },
            }
        );
    }

    return (
        <AuthenticatedLayout user={auth.user}>
            <nav style={{display:'none'}} className="top-bar">
                <i className="bi bi-trophy-fill" id="trophy-logo"></i>
                <Button variant="link" id="rankings-link">Rankings</Button>
                <Image src="" alt="User profile logo" id="user-logo" roundedCircle/>
                <span id="username">
                    {auth.user ? auth.user.username : (<>You are not logged in</>)}
                </span>
            </nav>
            <Container style={{marginTop: '20px'}}>
                <h1 id="title">OsiChess</h1>
                <h2 id="subtitle">Really bad chess</h2>
                <br></br>
                <Row style={{display:'none'}}>
                    <Col id="col-content" sm={12} md={6}>
                        <br></br>
                        <h5>Singleplayer</h5>
                        <br></br>
                        <Form.Label for="hours-input">Insert game time hours</Form.Label>
                        <Form.Control type="number" placeholder="Insert game time hours" id="hours-input" min="0"/>
                        <br></br>
                        <Form.Label for="minutes-input">Insert game time minutes</Form.Label>
                        <Form.Control type="number" placeholder="Insert game time minutes" id="minutes-input" min="0"/>
                        <br></br>
                        <Form.Select aria-label="Difficulty selection" id="difficulty-select">
                            <option>Select a difficulty level</option>
                            <option value="1">Easy</option>
                            <option value="2">Medium</option>
                            <option value="3">Hard</option>
                        </Form.Select>
                        <br></br>
                        <Button variant="primary">Start match</Button>
                    </Col>
                    <Col id="col-content" sm={12} md={6}>
                        <br></br>
                        <h5>Multiplayer</h5>
                        <br></br>
                        <Button variant="primary">Create a new game</Button>
                        <br></br><br></br>
                        <Button variant="primary">Join a game</Button>
                    </Col>
                </Row>
            </Container>
            <div style={{display:'flex', justifyContent:'center'}}>
                <Button onClick={newGame} variant="primary">Start match</Button>
            </div>
        </AuthenticatedLayout>
    );
}
