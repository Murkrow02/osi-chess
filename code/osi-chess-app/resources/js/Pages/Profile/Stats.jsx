import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';

export default function Stats({ auth, stats }) {

    console.log(stats);

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Profile</h2>}
        >
            <Head title="Stats" />
            <h1>!%</h1>
            {
                stats.map((stat) => {
                    return (
                        <div style={{display:'flex', gap:'10px'}}>
                            <p>{stat.created_at}</p>
                            <p>{stat.moves_count}</p>
                            <p>{stat.winner? "W" : "L"}</p>
                        </div>
                    );
                })
            }

        </AuthenticatedLayout>
    );
}
