import { useState } from 'react';
import ApplicationLogo from '@/Components/ApplicationLogo';
import Dropdown from '@/Components/Dropdown';
import NavLink from '@/Components/NavLink';
import ResponsiveNavLink from '@/Components/ResponsiveNavLink';
import { Link } from '@inertiajs/react';

export default function Authenticated({ user, header, children }) {
    const [showingNavigationDropdown, setShowingNavigationDropdown] = useState(false);
    console.log(user);
    return (


        <div>
            <nav className={'app-nav'}>

                {/* Logo */}
                <Link href={route('dashboard')}  style={{height: '100%'}}>
                    <ApplicationLogo/>
                </Link>


                {/* Navigation links */}

                {/* User drop-down */}
                {
                    user ?
                        <Dropdown>
                            <Dropdown.Trigger>
                        <span>
                            <button type="button">
                                {user.username}
                            </button>
                        </span>
                            </Dropdown.Trigger>

                            <Dropdown.Content>
                                <Dropdown.Link href={route('profile.edit')}>Profile</Dropdown.Link>
                                <Dropdown.Link href={route('profile.stats')}>Stats</Dropdown.Link>
                                <Dropdown.Link href={route('logout')} method="post" as="button">
                                    Log Out
                                </Dropdown.Link>
                            </Dropdown.Content>
                        </Dropdown>
                        :
                        <Link replace href={route('login')}>
                            <button type="button">Log in</button>
                        </Link>

                }


            </nav>

            {/* App */}
            <div>
                {children}
            </div>
        </div>
    );
}
