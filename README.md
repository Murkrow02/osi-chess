# SCACCHI

## Membri del team

- Marco Coppola 0001020433 marco.coppola3@studio.unibo.it                 RUOLO: Developer, Backend, Sysadmin
- Valerio Pio De Nicola 0001043353 valeriopio.denicola@studio.unibo.it    RUOLO: Product Owner, Frontend, Backend
- Chiara Tosadori 0001043695 chiara.tosadori@studio.unibo.it              RUOLO: Developer Frontend
- Bogdan Chirila  0001028516  bogdan.chirila@studio.unibo.it              RUOLO: Analista delle prestazioni
- Daniele Russo 0001028215 daniele.russo16@studio.unibo.it                RUOLO: Scrum Master, Frontend
- Ahmed Niouer  0001027288 ahmed.niouer@studio.unibo.it                   RUOLO: Developer, Frontend, UI

## Descrizione del sistema

L'obbiettivo è quello di realizzare un'applicazione web per giocare alla variante di scacchi denominata
"Really Bad Chess". L'applicazione permetterà all'utente di poter giocare sia contro un bot con vari livelli
di difficoltà che contro un altro giocatore. Durante la creazione della partita esiste un meccanismo
di stanze pubbliche (il primo che entra dal link generato gioca) e stanze private (si entra solo se invitato)
al termine della partita sarà possibile condivere il risultato su un social network e permettere quindi
ad altre persone di commentare.

Sarà inoltre possibile per chi crea una partita determinare la sua durata massima
per permettere ai giocatori di personalizzare la loro esperienza.

I risultati delle partite verranno salvati in una classifica e sarà sempre possibile per il giocatore vedere
i top player e la loro posizione globale che sarà principalmente basata sul numero di vittorie ottenute.
Sarà disponibile per il giocatore una sezione privata in cui potrà monitorare le sue statistiche e i suoi risultati
delle partite giocate contro altri giocatori, per verificare il suo miglioramento o peggioramento.
Tra le statistiche visibili nella classifica abbiamo deciso di includere:
numero di vittorie e sconfitte, numero di mosse impiegate per fare scacco matto, tempo impiegato per vincere.

## Note

- Comunicazione tra i membri del gruppo effettuata su un gruppo privato di Telegram.
- Gestione della repository Git effettuata tramite Gitlab (self-hosted) https://git.murkrowdev.org/
- Gestione delle user stories effettuata tramite Taiga (self-hosted) https://taiga.murkrowdev.org/
- Gestione delle task effettuata tramite Mattermost (self-hosted) https://mattermost.murkrowdev.org/
- Analisi del codice e delle vulnerabilità effettuate tramite SonarQube (self-hosted) https://sonarqube.murkrowdev.org/
- Uno Scrum circa ogni due giorni, alla fine di ogni scrum i membri del team hanno a disposizione delle task da completare
  scelte tutti insieme.

### Tech Stack
- Linguaggi di programmazione:
  - Front-end:
    - HTML
    - CSS
    - Javascript
  - Back-end:
    - PHP
- Librerie/Framework:
  - Front-end:
    - Framework:
      - React
    - Librerie:
      - chess.js:
        - https://github.com/jhlywa/chess.js/tree/master
      - chessboard.jsx:
        - https://github.com/willb335/chessboardjsx
      - MUI
        - https://mui.com/
  - Back-end:
    - Laravel
    - StockFish (chess bot)
      - https://stockfishchess.org
- DBMS:
  - MariaDB
- Utility:
  - Docker
    - Utilizzato per containerizzare tutti i servizi necessari per il progetto
  - draw.io
    - Utilizzato per realizzare lo schema architetturale del progetto
  - Figma
    - Utilizzato per realizzare il mockup dell'interfaccia grafica


## Gitflow

![Struttura di una repository Git organizzata secondo "gitflow"](./assets/gitflow.png)

Per ogni "task" del progetto Taiga, aprire una "issue" su questa repository di Gitlab e assegnarla al membro del team che deve
eseguirla.  
Per lo sviluppo di ogni issue, creare un branch di nome "#numeroissue" dal branch "development".  
Una volta che l'issue è stata completata, eseguire il merge di "#numeroissue" in "development".
Nominare i branch dei rilasci "release/sprint0", "release/sprint1", e così via, al posto di "release/V0.1.0".

## Installazione
Guardare il file README situato in code/osi-chess-app/README.md

## Guida ufficiale di Scrum: riassunto

Scrum è un metodo di sviluppo agile, semplice e leggero.  
Scrum è fondato sull'intelligenza collettiva delle persone.  
Le regole di Scrum NON forniscono istruzioni dettagliate, ma guidano le relazioni e le 
interazioni tra i membri del team.

### La teoria di Scrum

Scrum si basa su due concetti:
- empirismo: le decisioni vengono prese in base all'esperienza e alla storia passata;
- pensiero lean: ridurre gli sprechi e concentrarsi su ciò che è essenziale.
  
Scrum usa un approccio:
- iterativo: analisi dei requisiti, progettazione, codifica, testing e manutenzione vengono eseguiti iterativamente;
- incrementale: si consegnano spessissimo delle nuove versioni del prodotto software, dette incrementi, che aggiungono "passo-passo" un po'
  di feature che l'utente/cliente desidera.
  
Lo sviluppo del prodotto software viene diviso in periodi di tempo dalla durata costante, detti Sprint.  
Uno sprint dura tipicamente 2 o 4 settimane.  
  
All'interno di uno Sprint, si tengono tutti i 4 tipi di eventi fondamentali di Scrum:
- uno Sprint Planning il primo giorno;
- uno Scrum quotidiano;
- uno Sprint Review (magari l'ultimo giorno);
- uno Sprint Retrospective (magari l'ultimo giorno).
  
Questi eventi funzionano perchè implementano i pilastri empirici di Scrum:
- trasparenza;
- ispezione;
- adattamento.
  
### Trasparenza

Ogni membro del Team Scrum e ogni Stakeholder (persone esterne al team ma interessate al prodotto software, tra cui clienti, utenti, investitori, ...) DEVE avere la possibilità di vedere e capire tutto il processo di sviluppo.  
  
In poche parole, "tutti vedono tutto".  
  
La trasparenza è necessaria per permettere l'ispezione.  

### Ispezione

Gli artefatti di Scrum e l'avanzamento verso gli obiettivi concordati devono  
essere ispezionati frequentemente.  
Se si nota una deviazione dall'obiettivo che ci si è posti, è necessario modificare gli artefatti.  
    
Gli eventi dello Sprint permettono l'ispezione continua.
  
L'ispezione è necessaria per l'adattamento.  

### Adattamento

Se i requisiti (ciò che il cliente/utente vuole) del prodotto software cambiano o se il processo di sviluppo devia dall'obiettivo
che ci si è posti, bisogna adattarsi.  
  
L'adattamento diventa difficile se le persone non sono autogestite.  
Per questo, è fondamentale che ogni membro del Team Scrum sia autonomo.  
Nel Team Scrum NON può esserci un capo assoluto che detta legge e assegna i compiti agli altri membri.  

### I valori di Scrum

Il metodo Scrum funziona se i membri del Team Scrum seguono i seguenti 5 valori:
- impegno: i membri del Team Scrum si impegnano a raggiungere i loro obiettivi e a supportarsi a vicenda;
- focus: ogni membro del Team Scrum deve concentrarsi sull'obiettivo dello Sprint corrente e fare di tutto per raggiungerlo;
- apertura: ogni membro del Team Scrum e ogni Stakeholder deve essere aperto nei confronti degli altri; se c'è un problema, va spiegato
agli altri per affrontarlo insieme;
- rispetto: i membri del Team Scrum si rispettano reciprocamente come persone capaci e indipendenti; i membri del team devono essere rispettati come tali dagli Stakeholder;
- coraggio: ogni membro del Team Scrum deve avere il coraggio di fare la cosa giusta, anche se questa è la strada più difficile.
  
### Lo Scrum Team

Un piccolo gruppo di persone (minimo 5, massimo 10), che hanno un unico obiettivo: raggiungere il Product Goal.  
  
Uno Scrum Team è:
- cross-funzionale: i membri del team hanno tutte le competenze necessarie (back-end, front-end, database, UX/UI, design, ...) a creare valore (cose che il cliente/utente vuole) ad ogni Sprint;
- autogestito: i membri del team decidono INSIEME chi fa cosa, quando e come; NON c'è un capo che detta legge.
  
All'interno dello Scrum Team NON ci sono sottogruppi o gerarchie: tutti i membri sono alla pari.  
  
L'intero Scrum Team è responsabile di creare un Increment UTILE e DI VALORE ad OGNI SPRINT.  
  
I membri dello Scrum Team sono divisi nei seguenti 3 ruoli:
- un Product Owner (PO);
- uno Scrum Master (SM);
- Developer (tutti gli altri).

### Developer

Hanno il compito di implementare le feature di un Increment. 
  
I Developer sono sempre responsabili di:
- creare lo Sprint Backlog;
- aderire ad una Definition of Done;
- adattare quotidianamente la pianificazione allo Sprint Goal;
- ritenersi reciprocamente responsabili come professionisti.
  
### Il Product Owner
  
Egli fa da ponte tra lo Scrum Team e gli Stakeholder.  
Fa da porta-voce dello Scrum Team davanti agli Stakeholder.  
Comunica allo Scrum Team ciò che gli Stakeholder, in particolare i clienti/utenti, desiderano dal prodotto software.  
  
Inoltre, è responsabile della gestione del Product Backlog; la sua gestione include:
- sviluppare e comunicare al team il Product Goal;
- creare e comunicare gli elementi del Product Backlog;
- ordinare gli elementi del Product Backlog;
- assicurarsi che il Product Backlog sia trasparente, visibile e chiaro.
  
Il PO può eseguire tutte queste attività o delegarne alcune ad altri.  
In tutti i casi, egli ne rimane il responsabile.  
  
Tutti devono rispettare le decisioni del Product Owner.  
Nonostante ciò, ricordiamo che il PO NON è un capo che detta legge.  
  
Qualsiasi membro dello Scrum Team può proporre al Product Owner delle modifiche da applicare al Product Backlog.  
Bisogna ottenere il permesso del Product Owner per modificare il Product Backlog.  

### Lo Scrum Master

Egli ha il compito di aiutare tutti i membri dello Scrum Team a comprendere e ad usare il metodo Scrum.  
  
Lo Scrum Master fornisce un servizio allo Scrum Team in diversi modi:
- allenare i membri del team all'autogestione e alla cross-funzionalità;
- aiutare lo Scrum Team a concentrarsi nel creare Increment che incontrino la Definition of Done;
- eliminare gli impedimenti all'avanzamento dello Scrum Team;
- assicurare che tutti gli Eventi Scrum siano svolti in modo produttivo e che siano contenuti nello Sprint.
  
Lo Scrum Master fornisce un servizio al Product Owner in diversi modi:
- aiutarlo nel trovare tecniche per definire il Product Goal e costruire il Product Backlog;
- aiutare lo Scrum Team a definire elementi del Product Backlog chiari e concisi;
- facilitare la collaborazione con gli Stakeholder se richiesto o necessario.
  
Lo Scrum Master fornisce un servizio all'organizzazione (azienda che contiene lo Scrum Team) in diversi modi:
- assistere l'organizzazione nell'adozione di Scrum;
- rimuovere le barriere fra gli Stakeholder e lo Scrum Team.
  
### Gli Eventi Scrum

Lo Sprint è un contenitore per tutti gli altri Eventi Scrum.  
  
Ogni Evento Scrum è un occasione formale per ispezionare e adattare gli Artefatti Scrum.  
  
Tutti i membri dello Scrum Team dovrebbero partecipare ad ogni singolo evento.  
  
Un Evento Scrum si dovrebbe tenere nello stesso luogo e nello stesso momento.  
Se non è possibile che il luogo sia fisico, va bene anche una videoconferenza.  

### Lo Sprint

Sono il cuore pulsante di Scrum.  
  
Sono eventi di durata fissa (2 o 4 settimane).  
Un nuovo Sprint inizia subito dopo la fine dello Sprint precedente.  
  
Tutto il lavoro necessario per raggiungere il Product Goal si DEVE tenere all'interno degli Sprint.  
  
Durante lo Sprint:
- non sono apportate modifiche che potrebbero mettere a repentaglio lo Sprint Goal;
- la qualità non diminuisce;
- il Product Backlog viene perfezionato in base alle necessità;
- l’ambito (“scope”) può essere chiarito e rinegoziato con il Product Owner man mano che si
apprende di più.
  
Esistono varie pratiche per valutare l'avanzamento dello Sprint, come il burn-down chart.  
Queste pratiche NON sostituiscono l'importanza dell'empirismo: adattarsi in base all'esperienza passata.  
  
Il Product Owner può decidere di annullare lo Sprint corrente se lo Sprint Goal diventa obsoleto.  
SOLO lui può annullare lo Sprint corrente.  
  
### Lo Sprint Planning
  
Lo Sprint Planning dà il via allo Sprint.  
  
Lo Sprint Planning tratta i seguenti argomenti.

#### Perchè questo Sprint è di valore?

Il Product Owner e il resto del team discutono insieme per decidere lo Sprint Goal: l'obiettivo finale
da raggiungere entro la fine dello Sprint (esempio: prototipo dell'applicazione, gioco contro il bot in Scacchi, ...).
  
#### Cosa si può fare in questo Sprint?

Discutendo con il PO, i Developer selezionano alcuni elementi del Product Backlog per formare lo Sprint Backlog, cioè 
la lista delle cose da fare in questo Sprint.
  
Lo Scrum Team può perfezionare questi elementi durante il processo.  
  
#### Come si svolgerà il lavoro scelto?

Per ogni elemento dello Sprint Backlog, i Developer pianificano il lavoro necessario.
  
I Developer discutono insieme per assegnare un valore di Story Points ad ogni elemento dello Sprint Backlog.  
  
Gli Story Point sono una metrica relativa che indica quanto un elemento dello Sprint Backlog è "grande" e impegnativo da realizzare.
  
Durata di uno Sprint Planning: 8 ore per uno Sprint di 4 settimane.

### Daily Scrum

Il Daily Scrum è un evento giornaliero di 15 minuti che si tiene ad inizio giornata.  
  
Ogni membro dello Scrum Team risponde alle seguenti domande:
- cosa ho fatto ieri che ha aiutato il team a raggiungere lo Sprint Goal?
- cosa farò oggi per aiutare il team a raggiungere lo Sprint Goal?
- vedo dei problemi che impediscono il raggiungimento dello Sprint Goal?
  
Se necessario, si modifica lo Sprint Backlog. Possono essere aggiunti ad esso altri elementi dal Product Backlog
se ritenuto necessario o utile.

### Sprint Review

Lo Scrum Team mostra l'Increment dello Sprint agli Stakeholder chiave e discute l'avanzamento verso
il Product Goal.
  
Esempi di domande da fare agli Stakeholder chiave:
- Che domande hai?
- Come utilizzeresti questo prodotto?
- Cosa ti entusiasma dell'incremento del prodotto che abbiamo esaminato? Perché?
- Come ti senti riguardo all'incremento del prodotto che abbiamo esaminato? Perché?
- Se potessi cambiare una cosa del prodotto che abbiamo costruito, cosa cambieresti? Perché?
- Diciamo che una caratteristica dovrebbe essere eliminata. Quale funzionalità ottiene il tuo voto? Perché hai scelto questo?
- Qual è la tua caratteristica o caratteristica preferita di questo prodotto? Perché è il tuo preferito?
- Cosa odi veramente di questo prodotto? Perché?
- Da quale altro prodotto sul mercato dovremmo imparare? Perché?
- Come possiamo migliorare le future revisioni degli sprint?
  
### Sprint Retrospective

I membri dello Scrum Team discutono in privato di come è andato lo Sprint e di come si può migliorare.
  
Esempi di domande:
- Cosa abbiamo fatto bene, che se non discutiamo potremmo dimenticare?
- Cosa non è andato così bene?
- Cosa proviamo riguardo al prossimo sprint ora che abbiamo identificato questi problemi?
- Cosa abbiamo imparato?
- Cosa ci lascia ancora perplessi?
- Quali sono le cose più importanti che abbiamo imparato oggi?
- Qualcuno ha un "apprezzamento" da condividere con un altro membro del team?
- Dove siamo confusi o poco chiari su qualcuno degli argomenti di cui abbiamo discusso oggi?
- Siamo d'accordo su quali azioni intraprenderemo da questa retrospettiva dello sprint alla prossima pianificazione dello sprint?

### Gli Artefatti Scrum

Gli Artefatti sono gli oggetti prodotti seguendo il metodo Scrum.
  
Ogni Artefatto è associato ad un impegno, cioè ciò che l'Artefatto deve rispettare.
  
Gli Artefatti obbligatori sono:
- Product Backlog, il suo impegno è il Product Goal;
- Sprint Backlog, il suo impegno è lo Sprint Goal;
- Increment, il suo impegno è la Definition of Done.

### Product Backlog

Il Product Backlog è la lista delle cose da fare per raggiungere il Product Goal.
  
Ogni elemento del Product Backlog viene scritto tipicamente sotto forma di User Story.
  
Il Product Goal è l'obiettivo a lungo termine per lo Scrum Team.  
Può essere, per esempio, la visione del prodotto software.  
   
### Sprint Backlog

Rappresenta la lista delle cose da fare nello Sprint attuale.  
Viene creato prendendo elementi dal Product Backlog.  
  
Ogni elemento ha una sua valutazione in Story Points.  
  
NESSUN membro del team ha il diritto di assegnare a sua discrezione gli elementi della lista ai Developer.  
Ogni Developer sceglie gli elementi da implementare in base alle sue capacità.  
  
Lo Sprint Goal è l'obiettivo finale che ci si è posti per lo Sprint (esempio: creare la modalità PvB in Scacchi). 

### Increment

Un Increment è una nuova versione del prodotto software risultante dallo Sprint e che soddisfa lo Sprint Goal.
  
Un Increment è formato da tutte le feature implementate in questo Sprint integrate con tutte quelle implementate negli Sprint
precedenti.
  
Per poter essere considerato completato, un elemento dello Sprint Backlog deve soddisfare la Definition of Done.  
La Definition of Done è una definizione formale decisa dallo Scrum Team e/o dall'organizzazione e alla quale tutti i Developer
devono attenersi.
  
Un esempio di Definition of Done è la seguente: test di unità, test di componenti e test di integrazione superati.

### Burn-down chart

Artefatto Scrum FACOLTATIVO.  
Esso è un grafico che mostra l'andamento dello Scrum Team in termini di Story Point accumulati
quotidianamente, confrontato con l'andamento ideale che uno Scrum Team dovrebbe seguire.
  
Permette di capire se lo Scrum Team sta proseguendo bene.
  
Per quanto utile, NON è un sostituto dell'empirismo: adattarsi in base all'esperienza.

